INSERT INTO app_role (id, role_name, description) VALUES (1, 'STANDARD_USER', 'Standard User - Has no admin rights');
INSERT INTO app_role (id, role_name, description) VALUES (2, 'ADMIN_USER', 'Admin User - Has permission to perform admin tasks');


INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (1, 'John', 'Doe', '$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6', 'javainuse');
INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (2, 'Admin', 'Admin', '$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6', 'admin.admin');


INSERT INTO user_role(user_id, role_id) VALUES(1,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,2);


INSERT INTO public.estacionamiento (id, codigo) VALUES(1, 'UNGS');

INSERT INTO public.sector (id, codigo, estacionamiento_id) VALUES(1, 's1', 1);

INSERT INTO public.fila (id, codigo, sector_id) VALUES(1, 'A', 1);

INSERT INTO public.espacio (id, codigo, latitud, longitud, pos, three_words, fila_id) VALUES(1, '1', 1,1, '1', 'abc', 1);

