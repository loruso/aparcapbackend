package com.loruso.aparcap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AparcappApplication {

	public static void main(String[] args) {
		SpringApplication.run(AparcappApplication.class, args);
	}
}
