package com.loruso.aparcap.controller;

import com.loruso.aparcap.model.Espacio;
import com.loruso.aparcap.model.Role;
import com.loruso.aparcap.model.User;
import com.loruso.aparcap.repository.RoleRepository;
import com.loruso.aparcap.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RestController
public class UserController{
    @Autowired
    private UserRepository repository;
    @Autowired
    private RoleRepository roleRepository;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public List<User> getUsers() {
        return repository.findAll();
    }
    
    @PostMapping("/register")
    public User add(@RequestBody User entity) {
        ArrayList<Role> roles = new ArrayList<>();
        Optional<Role> optionalRole = roleRepository.findById((long)1);
        roles.add(optionalRole.get());
        entity.setRoles(roles);
        return repository.save(entity);
    }
}
