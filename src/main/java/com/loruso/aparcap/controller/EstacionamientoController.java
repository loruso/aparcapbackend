package com.loruso.aparcap.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.loruso.aparcap.model.Espacio;
import com.loruso.aparcap.model.Estacionamiento;
import com.loruso.aparcap.repository.EspacioRepository;
import com.loruso.aparcap.repository.EstacionamientoRepository;

@RestController
public class EstacionamientoController {


	@Autowired
	private EstacionamientoRepository repository;

	@GetMapping("/estacionamiento/{id}")
	public Optional<Estacionamiento> get(@PathVariable("id") int id) {
		return repository.findById(id);
	}

	@GetMapping("/estacionamiento")
	public List<Estacionamiento> all() {
		return repository.findAll();
	}

	@PostMapping("/estacionamiento")
	public Estacionamiento add(@RequestBody Estacionamiento entity) {
		return repository.save(entity);
	}

	@DeleteMapping("/estacionamiento/{id}")
	public List<Estacionamiento> remove(@PathVariable("id") int id) {
		repository.deleteById(id);
		return repository.findAll();
	}

	@PutMapping("/estacionamiento")
	public Estacionamiento update(@RequestBody Estacionamiento entity) {
		return repository.save(entity);
	}
}