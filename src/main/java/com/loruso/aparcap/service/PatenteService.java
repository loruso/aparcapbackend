package com.loruso.aparcap.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.loruso.aparcap.model.Patente;
import com.loruso.aparcap.repository.PatenteRepository;

@Service
public class PatenteService implements CrudService<Patente> {

	@Autowired
	private PatenteRepository repository;

	@Override
	public ResponseEntity<Patente> add(Patente entity) {
		repository.save(entity);
		return ResponseEntity.ok(entity);
	}

	@Override
	public ResponseEntity<Patente> remove(Patente entity) {
		Optional<Patente> PatenteOpcional = repository.findById(entity.getId());

		if (!PatenteOpcional.isPresent())
			return ResponseEntity.noContent().build();

		repository.delete(entity);
		return ResponseEntity.ok().build();
	}

	@Override
	public ResponseEntity<Patente> remove(int id) {
		Optional<Patente> PatenteOpcional = repository.findById(id);

		if (!PatenteOpcional.isPresent())
			return ResponseEntity.noContent().build();

		repository.deleteById(id);
		return ResponseEntity.ok().build();
	}

	@Override
	public ResponseEntity<Patente> update(Patente entity) {
		Optional<Patente> PatenteOptional = repository.findById(entity.getId());

		if (!PatenteOptional.isPresent())
			return ResponseEntity.noContent().build();

		repository.save(entity);
		return ResponseEntity.status(HttpStatus.OK).body(entity);
	}

	@Override
	public ResponseEntity<List<Patente>> all() {
		return ResponseEntity.ok(repository.findAll());	
	}

	@Override
	public ResponseEntity<Patente> getById(int id) {
		Optional<Patente> PatenteOptional = repository.findById(id);

		if (!PatenteOptional.isPresent())
			return ResponseEntity.noContent().build();

		return ResponseEntity.ok(PatenteOptional.get());
	}
}
