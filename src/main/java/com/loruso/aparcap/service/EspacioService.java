package com.loruso.aparcap.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.loruso.aparcap.model.Espacio;
import com.loruso.aparcap.repository.EspacioRepository;

@Service
public class EspacioService implements CrudService<Espacio> {

	@Autowired
	private EspacioRepository repository;

	@Override
	public ResponseEntity<Espacio> add(Espacio entity) {
		repository.save(entity);
		return ResponseEntity.ok(entity);
	}

	@Override
	public ResponseEntity<Espacio> remove(Espacio entity) {
		Optional<Espacio> espacioOpcional = repository.findById(entity.getId());

		if (!espacioOpcional.isPresent())
			return ResponseEntity.noContent().build();

		repository.delete(entity);
		return ResponseEntity.ok().build();
	}

	@Override
	public ResponseEntity<Espacio> remove(int id) {
		Optional<Espacio> espacioOpcional = repository.findById(id);

		if (!espacioOpcional.isPresent())
			return ResponseEntity.noContent().build();

		repository.deleteById(id);
		return ResponseEntity.ok().build();
	}

	@Override
	public ResponseEntity<Espacio> update(Espacio entity) {
		Optional<Espacio> EspacioOptional = repository.findById(entity.getId());

		if (!EspacioOptional.isPresent())
			return ResponseEntity.noContent().build();

		repository.save(entity);
		return ResponseEntity.status(HttpStatus.OK).body(entity);
	}

	@Override
	public ResponseEntity<List<Espacio>> all() {
		return ResponseEntity.ok(repository.findAll());	
	}

	@Override
	public ResponseEntity<Espacio> getById(int id) {
		Optional<Espacio> EspacioOptional = repository.findById(id);

		if (!EspacioOptional.isPresent())
			return ResponseEntity.noContent().build();

		return ResponseEntity.ok(EspacioOptional.get());
	}
}
