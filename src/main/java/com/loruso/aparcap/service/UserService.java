package com.loruso.aparcap.service;

import com.loruso.aparcap.model.User;
import com.loruso.aparcap.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class UserService implements CrudService<User> {
    @Autowired
    private UserRepository repository;

    @Override
    public ResponseEntity<User> add(User entity) {
    	// TODO: Acá hay que encriptar la contraseña antes de guardarla, Bcrypt.
    	repository.save(entity);
        return ResponseEntity.ok(entity);
    }

    @Override
    public ResponseEntity<User> remove(User entity) {
        return null;
    }

    @Override
    public ResponseEntity<User> remove(int id) {
        return null;
    }

    @Override
    public ResponseEntity<User> update(User entity) {
        return null;
    }

    @Override
    public ResponseEntity<List<User>> all() {
        return null;
    }

    @Override
    public ResponseEntity<User> getById(int id) {
        return null;
    }
}
