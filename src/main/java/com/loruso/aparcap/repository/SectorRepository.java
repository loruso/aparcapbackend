package com.loruso.aparcap.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.loruso.aparcap.model.Espacio;
import com.loruso.aparcap.model.Sector;

public interface SectorRepository extends JpaRepository<Sector, Integer>{}

