package com.loruso.aparcap.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.loruso.aparcap.model.Patente;

public interface PatenteRepository extends JpaRepository<Patente, Integer>{}
