package com.loruso.aparcap.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.loruso.aparcap.model.Espacio;

public interface EspacioRepository extends JpaRepository<Espacio, Integer>{}

