package com.loruso.aparcap.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.loruso.aparcap.model.Estacionamiento;

public interface EstacionamientoRepository extends JpaRepository<Estacionamiento, Integer>{}

