package com.loruso.aparcap.repository;


import com.loruso.aparcap.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
