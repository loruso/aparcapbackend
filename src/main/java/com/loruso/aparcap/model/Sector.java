package com.loruso.aparcap.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "sector")
public class Sector {

	@Id
    private Long id;
	private String codigo;
	@OneToMany(mappedBy="sector")
	private Set<Fila> filas;
	@JsonIgnore
	@ManyToOne
    @JoinColumn(name = "estacionamiento_id")
	private Estacionamiento estacionamiento;
		
	public Sector() {};
	public Sector(Long id, Set<Fila> filas) {
		super();
		this.id = id;
		this.filas = filas;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Set<Fila> getFilas() {
		return filas;
	}

	public void setFilas(Set<Fila> filas) {
		this.filas = filas;
	}

	public Estacionamiento getEstacionamiento() {
		return estacionamiento;
	}

	public void setEstacionamiento(Estacionamiento estacionamiento) {
		this.estacionamiento = estacionamiento;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	
	
	
}
