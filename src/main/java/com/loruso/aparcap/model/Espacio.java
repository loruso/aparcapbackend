package com.loruso.aparcap.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "espacio")
public class Espacio {
	@Id
    private int id;
	private long latitud;
	private long longitud;
	private String threeWords;
	private String codigo;
	private String pos;
	@JsonIgnore
	@ManyToOne
    @JoinColumn(name = "fila_id")
	private Fila fila;
	
	
	public Espacio() {}
		
	
	public Espacio(long latitud, long longitud, String threeWords, String codigo, String fila, String pos) {
		super();
		this.latitud = latitud;
		this.longitud = longitud;
		this.threeWords = threeWords;
		this.codigo = codigo;
		this.pos = pos;
	}
	
	public Fila getFila() {
		return fila;
	}

	public void setFila(Fila fila) {
		this.fila = fila;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public int getId() {
		return id;
	}	

	public void setId(int id) {
		this.id = id;
	}

	public long getLatitud() {
		return latitud;
	}

	public void setLatitud(long latitud) {
		this.latitud = latitud;
	}

	public long getLongitud() {
		return longitud;
	}

	public void setLongitud(long longitud) {
		this.longitud = longitud;
	}

	public String getThreeWords() {
		return threeWords;
	}

	public void setThreeWords(String threeWords) {
		this.threeWords = threeWords;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
}
