package com.loruso.aparcap.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "estacionamiento")
public class Estacionamiento {

	@Id
    private Long id;
	private String codigo;
	@OneToMany(mappedBy="estacionamiento")
	private Set<Sector> sectores;
	
	public Estacionamiento() {};
	
	public Estacionamiento(Long id, Set<Sector> sectores) {
		super();
		this.id = id;
		this.sectores = sectores;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    
	public Set<Sector> getSectores() {
		return sectores;
	}
	public void setSectores(Set<Sector> sectores) {
		this.sectores = sectores;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
}
