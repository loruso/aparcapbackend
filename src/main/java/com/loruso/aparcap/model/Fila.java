package com.loruso.aparcap.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.internal.IgnoreForbiddenApisErrors;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "fila")
public class Fila {

	@Id
	private Long id;
	private String codigo;
	@OneToMany(mappedBy="fila")
	private Set<Espacio> espacios;
	@JsonIgnore
	@ManyToOne
    @JoinColumn(name = "sector_id")
	private Sector sector;
	
	public Fila() {};
	public Fila(Set<Espacio> espacios) {
		super();
		this.espacios = espacios;
	}

	public Set<Espacio> getEspacios() {
		return espacios;
	}

	
	public void setEspacios(Set<Espacio> espacios) {
		this.espacios = espacios;
	}

	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}	
	
	
	
}
